#  An Epidemilogical Study - Deliverable Assigment

Following project was done in a course for advanced scientific programming. In the project, we study [compartmental models in epidemiology](https://en.wikipedia.org/wiki/Compartmental_models_in_epidemiology). We are instructed to use a system of ODEs that compartmentlize the population into 3 main groups, and solve this system of ODEs a numerical method. We also explore the significance different parameters (like reproduction number, vaccination time and rate) have on number of infected individuals. 

The project includes code that numerically solves the system of ODEs (using scipy's intergrate module) and plots to visualize the results.

The model and results of this model can be found in either the HTML-file of the Jupyter Notebook, or in a Jupyter Notebook file (see files under main-branch)
